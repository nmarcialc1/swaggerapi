# DovlyApi.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientKey** | **String** |  | [optional] 
**profileId** | **String** |  | [optional] 
**paymentProfileId** | **Number** |  | [optional] 
**emailOptOut** | **Boolean** |  | [optional] 
**city** | **String** |  | [optional] 
**zip** | **String** |  | [optional] 
**phoneOptOut** | **Boolean** |  | [optional] 
**productId** | **Number** |  | [optional] 
