# DovlyApi.StateApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**states**](StateApi.md#states) | **GET** /states | 
[**statesstate**](StateApi.md#statesstate) | **GET** /states/{state} | 

<a name="states"></a>
# **states**
> Object states()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.StateApi();
apiInstance.states((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="statesstate"></a>
# **statesstate**
> Object statesstate(state)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.StateApi();
let state = "state_example"; // String | find an specific state

apiInstance.statesstate(state, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **state** | **String**| find an specific state | 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

