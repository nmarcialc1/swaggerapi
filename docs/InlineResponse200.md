# DovlyApi.InlineResponse200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Alert ID | [optional] 
**userId** | **Number** | Dovly user id | [optional] 
**isPd** | **Boolean** |  | [optional] 
**alertId** | **Number** | External ID | [optional] 
**status** | **String** | 0&#x3D;unseen, 1&#x3D;seen, 2&#x3D;read | [optional] 
**bureau** | **String** | Associated credit bureau | [optional] 
**clientKey** | **String** |  | [optional] 
**requestKey** | **String** |  | [optional] 
**watchAlertType** | **String** | Type of alert | [optional] 
**watchAlertId** | **Number** | External ID | [optional] 
**alertDetail** | **String** | Alert details | [optional] 
**createdAt** | **Date** | When the alert was created | [optional] 
**updatedAt** | **Date** | When the alert was last updated | [optional] 

<a name="StatusEnum"></a>
## Enum: StatusEnum

* `0` (value: `"0"`)
* `1` (value: `"1"`)
* `2` (value: `"2"`)


<a name="BureauEnum"></a>
## Enum: BureauEnum

* `TransUnion` (value: `"TransUnion"`)
* `Experian` (value: `"Experian"`)
* `Equifax` (value: `"Equifax"`)

