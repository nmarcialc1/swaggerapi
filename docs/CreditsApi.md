# DovlyApi.CreditsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**creditalerts**](CreditsApi.md#creditalerts) | **GET** /credit/alerts | 
[**creditreportreportId**](CreditsApi.md#creditreportreportId) | **GET** /credit/report/{report-id} | 
[**creditreports**](CreditsApi.md#creditreports) | **GET** /credit/reports | 

<a name="creditalerts"></a>
# **creditalerts**
> InlineResponse200 creditalerts(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.CreditsApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to retrieves all credits alerts.
};
apiInstance.creditalerts(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves all credits alerts. | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="creditreportreportId"></a>
# **creditreportreportId**
> Object creditreportreportId(reportId, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.CreditsApi();
let reportId = 56; // Number | Report ID to retrieve the credit report information.
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito ID to retrieve the credit report information.
};
apiInstance.creditreportreportId(reportId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **reportId** | **Number**| Report ID to retrieve the credit report information. | 
 **userCognitoId** | [**StringType**](.md)| Cognito ID to retrieve the credit report information. | [optional] 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="creditreports"></a>
# **creditreports**
> Object creditreports(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.CreditsApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to retrieves all credits reports.
};
apiInstance.creditreports(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves all credits reports. | [optional] 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

