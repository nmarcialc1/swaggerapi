# DovlyApi.UsersApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteUserByTypeId**](UsersApi.md#deleteUserByTypeId) | **DELETE** /users/{id} | 
[**getCustomers**](UsersApi.md#getCustomers) | **GET** /users/customers/{status} | 
[**getLogins**](UsersApi.md#getLogins) | **GET** /users/logins/{cognito-id} | 
[**getOww**](UsersApi.md#getOww) | **GET** /users/oow/{cognito-id} | 
[**getUserByTypeId**](UsersApi.md#getUserByTypeId) | **GET** /users/{id} | 
[**getUserLeads**](UsersApi.md#getUserLeads) | **GET** /users/leads/{status} | 
[**putOww**](UsersApi.md#putOww) | **PUT** /users/oow/{cognito-id} | 
[**putUser**](UsersApi.md#putUser) | **PUT** /users/{id} | 

<a name="deleteUserByTypeId"></a>
# **deleteUserByTypeId**
> deleteUserByTypeId(id, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let id = "id_example"; // String | ID of the user to delete.
let opts = { 
  'idType': "" // String | Type of ID to delete the user information(id,cognito_id,uuid).
};
apiInstance.deleteUserByTypeId(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID of the user to delete. | 
 **idType** | **String**| Type of ID to delete the user information(id,cognito_id,uuid). | [optional] 

### Return type

null (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getCustomers"></a>
# **getCustomers**
> getCustomers(status)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let status = "status_example"; // String | find customers by status

apiInstance.getCustomers(status, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **String**| find customers by status | 

### Return type

null (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLogins"></a>
# **getLogins**
> getLogins(cognitoId)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let cognitoId = "cognitoId_example"; // String | find login attempts by cognitoId

apiInstance.getLogins(cognitoId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cognitoId** | **String**| find login attempts by cognitoId | 

### Return type

null (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getOww"></a>
# **getOww**
> getOww(cognitoId, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let cognitoId = "cognitoId_example"; // String | Cognito id to retrieves Out of Wallet questions from TransUnion.
let opts = { 
  'ssn': "" // String | Security Social number, matches regex ^[0-9]+$ and has a length of 4.
};
apiInstance.getOww(cognitoId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cognitoId** | **String**| Cognito id to retrieves Out of Wallet questions from TransUnion. | 
 **ssn** | **String**| Security Social number, matches regex ^[0-9]+$ and has a length of 4. | [optional] 

### Return type

null (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getUserByTypeId"></a>
# **getUserByTypeId**
> UserResponse getUserByTypeId(id, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let id = "id_example"; // String | user-id to retrieves the user info.
let opts = { 
  'idType': "" // String | Type of Id to retrieve the user data (id,cognito_id,uuid).
};
apiInstance.getUserByTypeId(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| user-id to retrieves the user info. | 
 **idType** | **String**| Type of Id to retrieve the user data (id,cognito_id,uuid). | [optional] 

### Return type

[**UserResponse**](UserResponse.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserLeads"></a>
# **getUserLeads**
> getUserLeads(status)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let status = "status_example"; // String | find leads filtered by status

apiInstance.getUserLeads(status, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **status** | **String**| find leads filtered by status | 

### Return type

null (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putOww"></a>
# **putOww**
> putOww(cognitoId, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let cognitoId = "cognitoId_example"; // String | Cognito id to retrieves the response of TransUnion.
let opts = { 
  'question': "", // String | JSON list of questions.
  'answer': "" // String | JSON list of answers.
};
apiInstance.putOww(cognitoId, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cognitoId** | **String**| Cognito id to retrieves the response of TransUnion. | 
 **question** | **String**| JSON list of questions. | [optional] 
 **answer** | **String**| JSON list of answers. | [optional] 

### Return type

null (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="putUser"></a>
# **putUser**
> putUser(bodyid, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.UsersApi();
let body = new DovlyApi.User(); // User | Info that needs to be updated to the user
let id = "id_example"; // String | ID of the user to update.
let opts = { 
  'idType': "" // String | Type of ID to update the user information(id,cognito_id,uuid).
};
apiInstance.putUser(bodyid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**User**](User.md)| Info that needs to be updated to the user | 
 **id** | **String**| ID of the user to update. | 
 **idType** | **String**| Type of ID to update the user information(id,cognito_id,uuid). | [optional] 

### Return type

null (empty response body)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

