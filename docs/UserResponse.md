# DovlyApi.UserResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**uuid** | **String** |  | [optional] 
**roleId** | **Number** |  | [optional] 
**clientKey** | **String** |  | [optional] 
**funnelId** | **Number** |  | [optional] 
**profileId** | **String** |  | [optional] 
**paymentProfileId** | **String** |  | [optional] 
**firstname** | **String** |  | [optional] 
**lastname** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**emailOptOut** | **Number** |  | [optional] 
**street** | **String** |  | [optional] 
**address** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**company** | **String** |  | [optional] 
**position** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**ssn** | **String** |  | [optional] 
**dob** | **String** |  | [optional] 
**phone** | **String** |  | [optional] 
**phoneOptOut** | **Number** |  | [optional] 
**zip** | **String** |  | [optional] 
**status** | **Number** |  | [optional] 
**accountStatus** | **String** |  | [optional] 
**isAuthenticated** | **Number** |  | [optional] 
**productId** | **Number** |  | [optional] 
**authType** | **String** |  | [optional] 
**overriddenDecline** | **Number** |  | [optional] 
**initiallyDeclined** | **Number** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**updatedAt** | **Date** |  | [optional] 
**lastActivity** | **Date** |  | [optional] 
**lastLogin** | **Date** |  | [optional] 
**addedBy** | **Number** |  | [optional] 
**canDispute** | **Number** |  | [optional] 
**isVisited** | **Number** |  | [optional] 
**streetAddress** | **String** |  | [optional] 
**fullAddress** | **String** |  | [optional] 
