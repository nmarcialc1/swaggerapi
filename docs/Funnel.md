# DovlyApi.Funnel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**funnelId** | **Number** | Funnel id   | 
**productId** | **Number** | Product id | 
