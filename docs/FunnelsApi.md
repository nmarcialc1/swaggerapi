# DovlyApi.FunnelsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteFunnel**](FunnelsApi.md#deleteFunnel) | **DELETE** /funnels/{id} | 
[**deleteFunnelProducts**](FunnelsApi.md#deleteFunnelProducts) | **DELETE** /funnels/{id}/products/{product-id} | 
[**getFunnel**](FunnelsApi.md#getFunnel) | **GET** /funnels/{id} | 
[**getFunnelProduct**](FunnelsApi.md#getFunnelProduct) | **GET** /funnels/{id}/products/{product-id} | 
[**getFunnelProducts**](FunnelsApi.md#getFunnelProducts) | **GET** /funnels/{id}/products | 
[**getFunnels**](FunnelsApi.md#getFunnels) | **GET** /funnels | 
[**putFunnelProducts**](FunnelsApi.md#putFunnelProducts) | **PUT** /funnels/{id}/products/{product-id} | 
[**putFunnels**](FunnelsApi.md#putFunnels) | **PUT** /funnels/{id} | 
[**saveFunnels**](FunnelsApi.md#saveFunnels) | **POST** /funnels | 
[**saveFunnelsProduct**](FunnelsApi.md#saveFunnelsProduct) | **POST** /funnels/{id}/products/{product-id} | 

<a name="deleteFunnel"></a>
# **deleteFunnel**
> Funnel deleteFunnel(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = "id_example"; // String | ID of the funnel to delete.

apiInstance.deleteFunnel(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID of the funnel to delete. | 

### Return type

[**Funnel**](Funnel.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteFunnelProducts"></a>
# **deleteFunnelProducts**
> FunnelProduct deleteFunnelProducts(id, productId)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = "id_example"; // String | ID of the funnel to delete.
let productId = "productId_example"; // String | ID of the product to delete.

apiInstance.deleteFunnelProducts(id, productId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID of the funnel to delete. | 
 **productId** | **String**| ID of the product to delete. | 

### Return type

[**FunnelProduct**](FunnelProduct.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFunnel"></a>
# **getFunnel**
> Funnel getFunnel(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = 56; // Number | id funnel to search

apiInstance.getFunnel(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id funnel to search | 

### Return type

[**Funnel**](Funnel.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFunnelProduct"></a>
# **getFunnelProduct**
> FunnelProduct getFunnelProduct(id, productId)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = "id_example"; // String | ID of the funnel to search.
let productId = "productId_example"; // String | ID of the product to search.

apiInstance.getFunnelProduct(id, productId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID of the funnel to search. | 
 **productId** | **String**| ID of the product to search. | 

### Return type

[**FunnelProduct**](FunnelProduct.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFunnelProducts"></a>
# **getFunnelProducts**
> [FunnelProduct] getFunnelProducts(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = 56; // Number | id funnel to search

apiInstance.getFunnelProducts(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id funnel to search | 

### Return type

[**[FunnelProduct]**](FunnelProduct.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFunnels"></a>
# **getFunnels**
> [Funnel] getFunnels()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
apiInstance.getFunnels((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Funnel]**](Funnel.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putFunnelProducts"></a>
# **putFunnelProducts**
> FunnelProduct putFunnelProducts(id, productId)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = 56; // Number | Funnel Id.
let productId = 56; // Number | Product Id.

apiInstance.putFunnelProducts(id, productId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Funnel Id. | 
 **productId** | **Number**| Product Id. | 

### Return type

[**FunnelProduct**](FunnelProduct.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="putFunnels"></a>
# **putFunnels**
> Funnel putFunnels(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = 56; // Number | Funnel Id.

apiInstance.putFunnels(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Funnel Id. | 

### Return type

[**Funnel**](Funnel.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="saveFunnels"></a>
# **saveFunnels**
> Funnel saveFunnels()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
apiInstance.saveFunnels((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Funnel**](Funnel.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="saveFunnelsProduct"></a>
# **saveFunnelsProduct**
> FunnelProduct saveFunnelsProduct(id, productId)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.FunnelsApi();
let id = 56; // Number | Funnel Id.
let productId = 56; // Number | Product Id.

apiInstance.saveFunnelsProduct(id, productId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Funnel Id. | 
 **productId** | **Number**| Product Id. | 

### Return type

[**FunnelProduct**](FunnelProduct.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

