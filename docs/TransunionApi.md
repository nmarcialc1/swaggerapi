# DovlyApi.TransunionApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**transunion**](TransunionApi.md#transunion) | **GET** /transunion | 

<a name="transunion"></a>
# **transunion**
> Object transunion(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.TransunionApi();
let opts = { 
  'action': new DovlyApi.StringType(), // StringType | Type of action required for the third-party server
  'data': new DovlyApi.StringType() // StringType | data for send the request to the third-party server
};
apiInstance.transunion(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **action** | [**StringType**](.md)| Type of action required for the third-party server | [optional] 
 **data** | [**StringType**](.md)| data for send the request to the third-party server | [optional] 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

