# DovlyApi.Dispute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**round** | **Number** |  | [optional] 
**disputeReasonId** | **Number** |  | [optional] 
**tuStatus** | **Number** |  | [optional] 
**xpnStatus** | **Number** |  | [optional] 
**efxStatus** | **Number** |  | [optional] 
**tuResult** | **Number** |  | [optional] 
**xpnResult** | **Number** |  | [optional] 
**efxResult** | **Number** |  | [optional] 
**tuLetter** | **Number** |  | [optional] 
**xpnLetter** | **Number** |  | [optional] 
**efxLetter** | **Number** |  | [optional] 
**status** | **String** |  | [optional] 
**disputedAt** | **Number** |  | [optional] 
**editedAt** | **Number** |  | [optional] 
**submissionDate** | **Number** |  | [optional] 
