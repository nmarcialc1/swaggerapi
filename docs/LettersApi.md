# DovlyApi.LettersApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteLetters**](LettersApi.md#deleteLetters) | **DELETE** /disputes/letters/{id} | 
[**disputesletters**](LettersApi.md#disputesletters) | **GET** /disputes/letters | 
[**postLetters**](LettersApi.md#postLetters) | **POST** /disputes/letters | 
[**putLetters**](LettersApi.md#putLetters) | **PUT** /disputes/letters/{id} | 

<a name="deleteLetters"></a>
# **deleteLetters**
> Letter deleteLetters(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.LettersApi();
let id = "id_example"; // String | Id of the letter to delete.

apiInstance.deleteLetters(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Id of the letter to delete. | 

### Return type

[**Letter**](Letter.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="disputesletters"></a>
# **disputesletters**
> [Letter] disputesletters()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.LettersApi();
apiInstance.disputesletters((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Letter]**](Letter.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postLetters"></a>
# **postLetters**
> Letter postLetters()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.LettersApi();
apiInstance.postLetters((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Letter**](Letter.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="putLetters"></a>
# **putLetters**
> Letter putLetters(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.LettersApi();
let id = 56; // Number | Letter Id.

apiInstance.putLetters(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Letter Id. | 

### Return type

[**Letter**](Letter.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

