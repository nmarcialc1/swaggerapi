# DovlyApi.DocumentsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteDocumentByUserId**](DocumentsApi.md#deleteDocumentByUserId) | **DELETE** /users/documents/{id} | 
[**getDocuments**](DocumentsApi.md#getDocuments) | **GET** /users/documents | 
[**getTypes**](DocumentsApi.md#getTypes) | **GET** /users/documents/types | 
[**saveDocuments**](DocumentsApi.md#saveDocuments) | **POST** /users/documents | 

<a name="deleteDocumentByUserId"></a>
# **deleteDocumentByUserId**
> Document deleteDocumentByUserId(id, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DocumentsApi();
let id = "id_example"; // String | Document id to delete.
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | ID of the user to delete.
};
apiInstance.deleteDocumentByUserId(id, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Document id to delete. | 
 **userCognitoId** | [**StringType**](.md)| ID of the user to delete. | [optional] 

### Return type

[**Document**](Document.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDocuments"></a>
# **getDocuments**
> [Document] getDocuments(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DocumentsApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to retrieves all documents by user.
};
apiInstance.getDocuments(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves all documents by user. | [optional] 

### Return type

[**[Document]**](Document.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getTypes"></a>
# **getTypes**
> Object getTypes(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DocumentsApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to authorize.
};
apiInstance.getTypes(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to authorize. | [optional] 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="saveDocuments"></a>
# **saveDocuments**
> Document saveDocuments(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DocumentsApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to retrieves all documents by user.
};
apiInstance.saveDocuments(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves all documents by user. | [optional] 

### Return type

[**Document**](Document.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

