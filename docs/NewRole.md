# DovlyApi.NewRole

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**slug** | **String** | Slug name | 
**role** | **String** | Role description | 
