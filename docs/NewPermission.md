# DovlyApi.NewPermission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**route** | **String** | Permissions route | 
