# DovlyApi.DisputesApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteLetterCategories**](DisputesApi.md#deleteLetterCategories) | **DELETE** /disputes/letters/categories/{id} | 
[**getAllDisputes**](DisputesApi.md#getAllDisputes) | **GET** /disputes | 
[**getLetterCategories**](DisputesApi.md#getLetterCategories) | **GET** /disputes/letters/categories | 
[**getReasons**](DisputesApi.md#getReasons) | **GET** /disputes/reasons | 
[**postLetterCategories**](DisputesApi.md#postLetterCategories) | **POST** /disputes/letters/categories | 
[**putDispute**](DisputesApi.md#putDispute) | **PUT** /disputes/{id} | 
[**putLetterCategories**](DisputesApi.md#putLetterCategories) | **PUT** /disputes/letters/categories/{id} | 
[**putSchedule**](DisputesApi.md#putSchedule) | **PUT** /disputes/schedule | 

<a name="deleteLetterCategories"></a>
# **deleteLetterCategories**
> LetterCategory deleteLetterCategories(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DisputesApi();
let id = "id_example"; // String | ID of  the letter category.

apiInstance.deleteLetterCategories(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID of  the letter category. | 

### Return type

[**LetterCategory**](LetterCategory.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllDisputes"></a>
# **getAllDisputes**
> DisputesFiltered getAllDisputes(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DisputesApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType(), // StringType | Cognito id to retrieves all disputes by userId.
  'start': new DovlyApi.StringType(), // StringType | Value to skip a given number of results in the query.
  'length': new DovlyApi.StringType() // StringType | Value to limit the number of results returned from the query.
};
apiInstance.getAllDisputes(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves all disputes by userId. | [optional] 
 **start** | [**StringType**](.md)| Value to skip a given number of results in the query. | [optional] 
 **length** | [**StringType**](.md)| Value to limit the number of results returned from the query. | [optional] 

### Return type

[**DisputesFiltered**](DisputesFiltered.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLetterCategories"></a>
# **getLetterCategories**
> [LetterCategory] getLetterCategories()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DisputesApi();
apiInstance.getLetterCategories((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[LetterCategory]**](LetterCategory.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getReasons"></a>
# **getReasons**
> [Reason] getReasons(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DisputesApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to retrieves all disputes by userId.
};
apiInstance.getReasons(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves all disputes by userId. | [optional] 

### Return type

[**[Reason]**](Reason.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postLetterCategories"></a>
# **postLetterCategories**
> LetterCategory postLetterCategories()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DisputesApi();
apiInstance.postLetterCategories((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**LetterCategory**](LetterCategory.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="putDispute"></a>
# **putDispute**
> Object putDispute(bodyid, opts)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DisputesApi();
let body = new DovlyApi.Dispute(); // Dispute | Info that needs to be updated an specific dispute
let id = 56; // Number | Dispute ID to update.
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to retrieves a disputes by userId.
};
apiInstance.putDispute(bodyid, opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Dispute**](Dispute.md)| Info that needs to be updated an specific dispute | 
 **id** | **Number**| Dispute ID to update. | 
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves a disputes by userId. | [optional] 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="putLetterCategories"></a>
# **putLetterCategories**
> LetterCategory putLetterCategories(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.DisputesApi();
let id = 56; // Number | letter category to update.

apiInstance.putLetterCategories(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| letter category to update. | 

### Return type

[**LetterCategory**](LetterCategory.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="putSchedule"></a>
# **putSchedule**
> Object putSchedule(opts)



### Example
```javascript
import DovlyApi from 'dovly_api';

let apiInstance = new DovlyApi.DisputesApi();
let opts = { 
  'userCognitoId': new DovlyApi.StringType() // StringType | Cognito id to retrieves the user's schedule updated.
};
apiInstance.putSchedule(opts, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCognitoId** | [**StringType**](.md)| Cognito id to retrieves the user&#x27;s schedule updated. | [optional] 

### Return type

**Object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

