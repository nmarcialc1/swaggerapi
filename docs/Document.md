# DovlyApi.Document

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userId** | **Number** | User id | [optional] 
**name** | **String** | Document  name | [optional] 
**file** | **String** | Document file name | [optional] 
**type** | **Number** | Document type | [optional] 
**status** | **String** | Document status | [optional] 
**rejectReason** | **String** | Document Reject status | [optional] 

<a name="StatusEnum"></a>
## Enum: StatusEnum

* `1` (value: `"1"`)
* `2` (value: `"2"`)
* `3` (value: `"3"`)

