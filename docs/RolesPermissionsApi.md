# DovlyApi.RolesPermissionsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteRolesPermissions**](RolesPermissionsApi.md#deleteRolesPermissions) | **DELETE** /roles/{id}/permissions/{permission-id} | 
[**getRolesPermissions**](RolesPermissionsApi.md#getRolesPermissions) | **GET** /roles/{id}/permissions | 
[**postRolesPermissions**](RolesPermissionsApi.md#postRolesPermissions) | **POST** /roles/{id}/permissions/{permission-id} | 

<a name="deleteRolesPermissions"></a>
# **deleteRolesPermissions**
> Object deleteRolesPermissions(id, permissionId)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.RolesPermissionsApi();
let id = "id_example"; // String | Role Id to delete.
let permissionId = "permissionId_example"; // String | Permission Id to delete.

apiInstance.deleteRolesPermissions(id, permissionId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Role Id to delete. | 
 **permissionId** | **String**| Permission Id to delete. | 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getRolesPermissions"></a>
# **getRolesPermissions**
> Object getRolesPermissions(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.RolesPermissionsApi();
let id = "id_example"; // String | Role id to get permissions.

apiInstance.getRolesPermissions(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Role id to get permissions. | 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postRolesPermissions"></a>
# **postRolesPermissions**
> Object postRolesPermissions(id, permissionId)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.RolesPermissionsApi();
let id = "id_example"; // String | Role id to create relation with permissions.
let permissionId = "permissionId_example"; // String | Permission  id to create relation with role.

apiInstance.postRolesPermissions(id, permissionId, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Role id to create relation with permissions. | 
 **permissionId** | **String**| Permission  id to create relation with role. | 

### Return type

**Object**

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

