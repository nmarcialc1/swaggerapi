# DovlyApi.Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**userFriendlyName** | **String** |  | 
**friendlyName** | **String** |  | [optional] 
**alias** | **String** |  | 
**recurring** | **Number** |  | [optional] 
**isprivate** | **Number** |  | [optional] 
**description** | **String** |  | 
**priority** | **Number** |  | 
**price** | **Number** |  | 
**reportType** | **String** |  | [optional] 
**monitoring** | **Number** |  | [optional] 
**simulator** | **Number** |  | [optional] 
**enrollment** | **Number** |  | [optional] 
**idProtection** | **Number** |  | [optional] 
**fullReport** | **Number** |  | [optional] 
**deletedAt** | **Date** |  | [optional] 
