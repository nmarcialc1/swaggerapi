# DovlyApi.PermissionsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deletePermission**](PermissionsApi.md#deletePermission) | **DELETE** /permissions/{id} | 
[**getPermissions**](PermissionsApi.md#getPermissions) | **GET** /permissions | 
[**postpermission**](PermissionsApi.md#postpermission) | **POST** /permissions | 
[**putPermission**](PermissionsApi.md#putPermission) | **PUT** /permissions/{id} | 

<a name="deletePermission"></a>
# **deletePermission**
> Permission deletePermission(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.PermissionsApi();
let id = "id_example"; // String | ID of the permission to delete.

apiInstance.deletePermission(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID of the permission to delete. | 

### Return type

[**Permission**](Permission.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getPermissions"></a>
# **getPermissions**
> [Permission] getPermissions()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.PermissionsApi();
apiInstance.getPermissions((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Permission]**](Permission.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="postpermission"></a>
# **postpermission**
> Permission postpermission(route)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.PermissionsApi();
let route = "route_example"; // String | 

apiInstance.postpermission(route, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **route** | **String**|  | 

### Return type

[**Permission**](Permission.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="putPermission"></a>
# **putPermission**
> Permission putPermission(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.PermissionsApi();
let id = 56; // Number | Permission id to update.

apiInstance.putPermission(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Permission id to update. | 

### Return type

[**Permission**](Permission.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

