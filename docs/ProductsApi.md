# DovlyApi.ProductsApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteProduct**](ProductsApi.md#deleteProduct) | **DELETE** /products/{id} | 
[**products**](ProductsApi.md#products) | **GET** /products | 
[**productsid**](ProductsApi.md#productsid) | **GET** /products/{id} | 
[**putProduct**](ProductsApi.md#putProduct) | **PUT** /products/{id} | 
[**saveProduct**](ProductsApi.md#saveProduct) | **POST** /products | 

<a name="deleteProduct"></a>
# **deleteProduct**
> Product deleteProduct(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.ProductsApi();
let id = "id_example"; // String | ID of the product to delete.

apiInstance.deleteProduct(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID of the product to delete. | 

### Return type

[**Product**](Product.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="products"></a>
# **products**
> [Product] products()



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.ProductsApi();
apiInstance.products((error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Product]**](Product.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="productsid"></a>
# **productsid**
> Product productsid(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.ProductsApi();
let id = "id_example"; // String | find an specific product

apiInstance.productsid(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| find an specific product | 

### Return type

[**Product**](Product.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putProduct"></a>
# **putProduct**
> Product putProduct(id)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.ProductsApi();
let id = 56; // Number | Product Id.

apiInstance.putProduct(id, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| Product Id. | 

### Return type

[**Product**](Product.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="saveProduct"></a>
# **saveProduct**
> Product saveProduct(nameuserFriendlyNamefriendlyNamealiasrecurringisprivatedescriptionprioritypricereportTypemonitoringsimulatorenrollmentidProtectionfullReport)



### Example
```javascript
import DovlyApi from 'dovly_api';
let defaultClient = DovlyApi.ApiClient.instance;


let apiInstance = new DovlyApi.ProductsApi();
let name = "name_example"; // String | 
let userFriendlyName = "userFriendlyName_example"; // String | 
let friendlyName = "friendlyName_example"; // String | 
let alias = "alias_example"; // String | 
let recurring = 56; // Number | 
let isprivate = 56; // Number | 
let description = "description_example"; // String | 
let priority = 56; // Number | 
let price = 56; // Number | 
let reportType = "reportType_example"; // String | 
let monitoring = 56; // Number | 
let simulator = 56; // Number | 
let enrollment = 56; // Number | 
let idProtection = 56; // Number | 
let fullReport = 56; // Number | 

apiInstance.saveProduct(nameuserFriendlyNamefriendlyNamealiasrecurringisprivatedescriptionprioritypricereportTypemonitoringsimulatorenrollmentidProtectionfullReport, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  | 
 **userFriendlyName** | **String**|  | 
 **friendlyName** | **String**|  | 
 **alias** | **String**|  | 
 **recurring** | **Number**|  | 
 **isprivate** | **Number**|  | 
 **description** | **String**|  | 
 **priority** | **Number**|  | 
 **price** | **Number**|  | 
 **reportType** | **String**|  | 
 **monitoring** | **Number**|  | 
 **simulator** | **Number**|  | 
 **enrollment** | **Number**|  | 
 **idProtection** | **Number**|  | 
 **fullReport** | **Number**|  | 

### Return type

[**Product**](Product.md)

### Authorization

[BearerAuth](../README.md#BearerAuth)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

