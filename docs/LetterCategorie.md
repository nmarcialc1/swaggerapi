# DovlyApi.LetterCategorie

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**alias** | **String** |  | [optional] 
