/**
 * DOVLY API
 * APIs used to integrate with Dovly
 *
 * OpenAPI spec version: 1.0
 * Contact: softwaredev@dovly.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The LetterCategorie model module.
* @module model/LetterCategorie
* @version 1.0
*/
export default class LetterCategorie {
    /**
    * Constructs a new <code>LetterCategorie</code>.
    * @alias module:model/LetterCategorie
    * @class
    */

    constructor() {
        
        
        
    }

    /**
    * Constructs a <code>LetterCategorie</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/LetterCategorie} obj Optional instance to populate.
    * @return {module:model/LetterCategorie} The populated <code>LetterCategorie</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new LetterCategorie();
                        
            
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('alias')) {
                obj['alias'] = ApiClient.convertToType(data['alias'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {String} name
    */
    name = undefined;
    /**
    * @member {String} alias
    */
    alias = undefined;




}
