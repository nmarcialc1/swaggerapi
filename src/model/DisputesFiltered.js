/**
 * DOVLY API
 * APIs used to integrate with Dovly
 *
 * OpenAPI spec version: 1.0
 * Contact: softwaredev@dovly.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The DisputesFiltered model module.
* @module model/DisputesFiltered
* @version 1.0
*/
export default class DisputesFiltered {
    /**
    * Constructs a new <code>DisputesFiltered</code>.
    * @alias module:model/DisputesFiltered
    * @class
    */

    constructor() {
        
        
        
    }

    /**
    * Constructs a <code>DisputesFiltered</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/DisputesFiltered} obj Optional instance to populate.
    * @return {module:model/DisputesFiltered} The populated <code>DisputesFiltered</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new DisputesFiltered();
                        
            
        }
        return obj;
    }





}
