/**
 * DOVLY API
 * APIs used to integrate with Dovly
 *
 * OpenAPI spec version: 1.0
 * Contact: softwaredev@dovly.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
* The UserResponse model module.
* @module model/UserResponse
* @version 1.0
*/
export default class UserResponse {
    /**
    * Constructs a new <code>UserResponse</code>.
    * @alias module:model/UserResponse
    * @class
    */

    constructor() {
        
        
        
    }

    /**
    * Constructs a <code>UserResponse</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/UserResponse} obj Optional instance to populate.
    * @return {module:model/UserResponse} The populated <code>UserResponse</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new UserResponse();
                        
            
            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('uuid')) {
                obj['uuid'] = ApiClient.convertToType(data['uuid'], 'String');
            }
            if (data.hasOwnProperty('role_id')) {
                obj['role_id'] = ApiClient.convertToType(data['role_id'], 'Number');
            }
            if (data.hasOwnProperty('client_key')) {
                obj['client_key'] = ApiClient.convertToType(data['client_key'], 'String');
            }
            if (data.hasOwnProperty('funnel_id')) {
                obj['funnel_id'] = ApiClient.convertToType(data['funnel_id'], 'Number');
            }
            if (data.hasOwnProperty('profile_id')) {
                obj['profile_id'] = ApiClient.convertToType(data['profile_id'], 'String');
            }
            if (data.hasOwnProperty('payment_profile_id')) {
                obj['payment_profile_id'] = ApiClient.convertToType(data['payment_profile_id'], 'String');
            }
            if (data.hasOwnProperty('firstname')) {
                obj['firstname'] = ApiClient.convertToType(data['firstname'], 'String');
            }
            if (data.hasOwnProperty('lastname')) {
                obj['lastname'] = ApiClient.convertToType(data['lastname'], 'String');
            }
            if (data.hasOwnProperty('email')) {
                obj['email'] = ApiClient.convertToType(data['email'], 'String');
            }
            if (data.hasOwnProperty('email_opt_out')) {
                obj['email_opt_out'] = ApiClient.convertToType(data['email_opt_out'], 'Number');
            }
            if (data.hasOwnProperty('street')) {
                obj['street'] = ApiClient.convertToType(data['street'], 'String');
            }
            if (data.hasOwnProperty('address')) {
                obj['address'] = ApiClient.convertToType(data['address'], 'String');
            }
            if (data.hasOwnProperty('city')) {
                obj['city'] = ApiClient.convertToType(data['city'], 'String');
            }
            if (data.hasOwnProperty('company')) {
                obj['company'] = ApiClient.convertToType(data['company'], 'String');
            }
            if (data.hasOwnProperty('position')) {
                obj['position'] = ApiClient.convertToType(data['position'], 'String');
            }
            if (data.hasOwnProperty('state')) {
                obj['state'] = ApiClient.convertToType(data['state'], 'String');
            }
            if (data.hasOwnProperty('ssn')) {
                obj['ssn'] = ApiClient.convertToType(data['ssn'], 'String');
            }
            if (data.hasOwnProperty('dob')) {
                obj['dob'] = ApiClient.convertToType(data['dob'], 'String');
            }
            if (data.hasOwnProperty('phone')) {
                obj['phone'] = ApiClient.convertToType(data['phone'], 'String');
            }
            if (data.hasOwnProperty('phone_opt_out')) {
                obj['phone_opt_out'] = ApiClient.convertToType(data['phone_opt_out'], 'Number');
            }
            if (data.hasOwnProperty('zip')) {
                obj['zip'] = ApiClient.convertToType(data['zip'], 'String');
            }
            if (data.hasOwnProperty('status')) {
                obj['status'] = ApiClient.convertToType(data['status'], 'Number');
            }
            if (data.hasOwnProperty('account_status')) {
                obj['account_status'] = ApiClient.convertToType(data['account_status'], 'String');
            }
            if (data.hasOwnProperty('is_authenticated')) {
                obj['is_authenticated'] = ApiClient.convertToType(data['is_authenticated'], 'Number');
            }
            if (data.hasOwnProperty('product_id')) {
                obj['product_id'] = ApiClient.convertToType(data['product_id'], 'Number');
            }
            if (data.hasOwnProperty('auth_type')) {
                obj['auth_type'] = ApiClient.convertToType(data['auth_type'], 'String');
            }
            if (data.hasOwnProperty('overridden_decline')) {
                obj['overridden_decline'] = ApiClient.convertToType(data['overridden_decline'], 'Number');
            }
            if (data.hasOwnProperty('initially_declined')) {
                obj['initially_declined'] = ApiClient.convertToType(data['initially_declined'], 'Number');
            }
            if (data.hasOwnProperty('created_at')) {
                obj['created_at'] = ApiClient.convertToType(data['created_at'], 'Date');
            }
            if (data.hasOwnProperty('updated_at')) {
                obj['updated_at'] = ApiClient.convertToType(data['updated_at'], 'Date');
            }
            if (data.hasOwnProperty('last_activity')) {
                obj['last_activity'] = ApiClient.convertToType(data['last_activity'], 'Date');
            }
            if (data.hasOwnProperty('last_login')) {
                obj['last_login'] = ApiClient.convertToType(data['last_login'], 'Date');
            }
            if (data.hasOwnProperty('added_by')) {
                obj['added_by'] = ApiClient.convertToType(data['added_by'], 'Number');
            }
            if (data.hasOwnProperty('can_dispute')) {
                obj['can_dispute'] = ApiClient.convertToType(data['can_dispute'], 'Number');
            }
            if (data.hasOwnProperty('is_visited')) {
                obj['is_visited'] = ApiClient.convertToType(data['is_visited'], 'Number');
            }
            if (data.hasOwnProperty('street_address')) {
                obj['street_address'] = ApiClient.convertToType(data['street_address'], 'String');
            }
            if (data.hasOwnProperty('full_address')) {
                obj['full_address'] = ApiClient.convertToType(data['full_address'], 'String');
            }
        }
        return obj;
    }

    /**
    * @member {Number} id
    */
    id = undefined;
    /**
    * @member {String} uuid
    */
    uuid = undefined;
    /**
    * @member {Number} role_id
    */
    role_id = undefined;
    /**
    * @member {String} client_key
    */
    client_key = undefined;
    /**
    * @member {Number} funnel_id
    */
    funnel_id = undefined;
    /**
    * @member {String} profile_id
    */
    profile_id = undefined;
    /**
    * @member {String} payment_profile_id
    */
    payment_profile_id = undefined;
    /**
    * @member {String} firstname
    */
    firstname = undefined;
    /**
    * @member {String} lastname
    */
    lastname = undefined;
    /**
    * @member {String} email
    */
    email = undefined;
    /**
    * @member {Number} email_opt_out
    */
    email_opt_out = undefined;
    /**
    * @member {String} street
    */
    street = undefined;
    /**
    * @member {String} address
    */
    address = undefined;
    /**
    * @member {String} city
    */
    city = undefined;
    /**
    * @member {String} company
    */
    company = undefined;
    /**
    * @member {String} position
    */
    position = undefined;
    /**
    * @member {String} state
    */
    state = undefined;
    /**
    * @member {String} ssn
    */
    ssn = undefined;
    /**
    * @member {String} dob
    */
    dob = undefined;
    /**
    * @member {String} phone
    */
    phone = undefined;
    /**
    * @member {Number} phone_opt_out
    */
    phone_opt_out = undefined;
    /**
    * @member {String} zip
    */
    zip = undefined;
    /**
    * @member {Number} status
    */
    status = undefined;
    /**
    * @member {String} account_status
    */
    account_status = undefined;
    /**
    * @member {Number} is_authenticated
    */
    is_authenticated = undefined;
    /**
    * @member {Number} product_id
    */
    product_id = undefined;
    /**
    * @member {String} auth_type
    */
    auth_type = undefined;
    /**
    * @member {Number} overridden_decline
    */
    overridden_decline = undefined;
    /**
    * @member {Number} initially_declined
    */
    initially_declined = undefined;
    /**
    * @member {Date} created_at
    */
    created_at = undefined;
    /**
    * @member {Date} updated_at
    */
    updated_at = undefined;
    /**
    * @member {Date} last_activity
    */
    last_activity = undefined;
    /**
    * @member {Date} last_login
    */
    last_login = undefined;
    /**
    * @member {Number} added_by
    */
    added_by = undefined;
    /**
    * @member {Number} can_dispute
    */
    can_dispute = undefined;
    /**
    * @member {Number} is_visited
    */
    is_visited = undefined;
    /**
    * @member {String} street_address
    */
    street_address = undefined;
    /**
    * @member {String} full_address
    */
    full_address = undefined;




}
