/**
 * DOVLY API
 * APIs used to integrate with Dovly
 *
 * OpenAPI spec version: 1.0
 * Contact: softwaredev@dovly.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import NewProduct from './NewProduct';
import TrackData from './TrackData';

/**
* The Product model module.
* @module model/Product
* @version 1.0
*/
export default class Product {
    /**
    * Constructs a new <code>Product</code>.
    * @alias module:model/Product
    * @class
    * @extends module:model/TrackData
    * @param name {} 
    * @param userFriendlyName {} 
    * @param alias {} 
    * @param description {} 
    * @param priority {} 
    * @param price {} 
    */

    constructor(name, userFriendlyName, alias, description, priority, price) {
        TrackData.call(this);
        
        this['name'] = name;
        this['user_friendly_name'] = userFriendlyName;
        this['alias'] = alias;
        this['description'] = description;
        this['priority'] = priority;
        this['price'] = price;
        
    }

    /**
    * Constructs a <code>Product</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/Product} obj Optional instance to populate.
    * @return {module:model/Product} The populated <code>Product</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new Product();
            
            TrackData.constructFromObject(data, obj);
            
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('user_friendly_name')) {
                obj['user_friendly_name'] = ApiClient.convertToType(data['user_friendly_name'], 'String');
            }
            if (data.hasOwnProperty('friendly_name')) {
                obj['friendly_name'] = ApiClient.convertToType(data['friendly_name'], 'String');
            }
            if (data.hasOwnProperty('alias')) {
                obj['alias'] = ApiClient.convertToType(data['alias'], 'String');
            }
            if (data.hasOwnProperty('recurring')) {
                obj['recurring'] = ApiClient.convertToType(data['recurring'], 'Number');
            }
            if (data.hasOwnProperty('isprivate')) {
                obj['isprivate'] = ApiClient.convertToType(data['isprivate'], 'Number');
            }
            if (data.hasOwnProperty('description')) {
                obj['description'] = ApiClient.convertToType(data['description'], 'String');
            }
            if (data.hasOwnProperty('priority')) {
                obj['priority'] = ApiClient.convertToType(data['priority'], 'Number');
            }
            if (data.hasOwnProperty('price')) {
                obj['price'] = ApiClient.convertToType(data['price'], 'Number');
            }
            if (data.hasOwnProperty('report_type')) {
                obj['report_type'] = ApiClient.convertToType(data['report_type'], 'String');
            }
            if (data.hasOwnProperty('monitoring')) {
                obj['monitoring'] = ApiClient.convertToType(data['monitoring'], 'Number');
            }
            if (data.hasOwnProperty('simulator')) {
                obj['simulator'] = ApiClient.convertToType(data['simulator'], 'Number');
            }
            if (data.hasOwnProperty('enrollment')) {
                obj['enrollment'] = ApiClient.convertToType(data['enrollment'], 'Number');
            }
            if (data.hasOwnProperty('id_protection')) {
                obj['id_protection'] = ApiClient.convertToType(data['id_protection'], 'Number');
            }
            if (data.hasOwnProperty('full_report')) {
                obj['full_report'] = ApiClient.convertToType(data['full_report'], 'Number');
            }
            if (data.hasOwnProperty('deleted_at')) {
                obj['deleted_at'] = ApiClient.convertToType(data['deleted_at'], 'Date');
            }
        }
        return obj;
    }

    /**
    * @member {String} name
    */
    name = undefined;
    /**
    * @member {String} user_friendly_name
    */
    user_friendly_name = undefined;
    /**
    * @member {String} friendly_name
    */
    friendly_name = undefined;
    /**
    * @member {String} alias
    */
    alias = undefined;
    /**
    * @member {Number} recurring
    */
    recurring = undefined;
    /**
    * @member {Number} isprivate
    */
    isprivate = undefined;
    /**
    * @member {String} description
    */
    description = undefined;
    /**
    * @member {Number} priority
    */
    priority = undefined;
    /**
    * @member {Number} price
    */
    price = undefined;
    /**
    * @member {String} report_type
    */
    report_type = undefined;
    /**
    * @member {Number} monitoring
    */
    monitoring = undefined;
    /**
    * @member {Number} simulator
    */
    simulator = undefined;
    /**
    * @member {Number} enrollment
    */
    enrollment = undefined;
    /**
    * @member {Number} id_protection
    */
    id_protection = undefined;
    /**
    * @member {Number} full_report
    */
    full_report = undefined;
    /**
    * @member {Date} deleted_at
    */
    deleted_at = undefined;




}
