/**
 * DOVLY API
 * APIs used to integrate with Dovly
 *
 * OpenAPI spec version: 1.0
 * Contact: softwaredev@dovly.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from "../ApiClient";
import StringType from '../model/StringType';

/**
* Transunion service.
* @module api/TransunionApi
* @version 1.0
*/
export default class TransunionApi {

    /**
    * Constructs a new TransunionApi. 
    * @alias module:api/TransunionApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }

    /**
     * Callback function to receive the result of the transunion operation.
     * @callback module:api/TransunionApi~transunionCallback
     * @param {String} error Error message, if any.
     * @param {Object} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * @param {Object} opts Optional parameters
     * @param {module:api/TransunionApi~transunionCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Object}
     */
    transunion(opts, callback) {
      opts = opts || {};
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
        'action': opts['action'],
        'data': opts['data']
      };
      let formParams = {
      };

      let authNames = ['BearerAuth'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = Object;

      return this.apiClient.callApi(
        '/transunion', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, callback
      );
    }

}
