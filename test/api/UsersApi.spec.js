/**
 * DOVLY API
 * APIs used to integrate with Dovly
 *
 * OpenAPI spec version: 1.0
 * Contact: softwaredev@dovly.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.DovlyApi);
  }
}(this, function(expect, DovlyApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new DovlyApi.UsersApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('UsersApi', function() {
    describe('deleteUserByTypeId', function() {
      it('should call deleteUserByTypeId successfully', function(done) {
        //uncomment below and update the code to test deleteUserByTypeId
        //instance.deleteUserByTypeId(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getCustomers', function() {
      it('should call getCustomers successfully', function(done) {
        //uncomment below and update the code to test getCustomers
        //instance.getCustomers(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getLogins', function() {
      it('should call getLogins successfully', function(done) {
        //uncomment below and update the code to test getLogins
        //instance.getLogins(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getOww', function() {
      it('should call getOww successfully', function(done) {
        //uncomment below and update the code to test getOww
        //instance.getOww(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getUserByTypeId', function() {
      it('should call getUserByTypeId successfully', function(done) {
        //uncomment below and update the code to test getUserByTypeId
        //instance.getUserByTypeId(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getUserLeads', function() {
      it('should call getUserLeads successfully', function(done) {
        //uncomment below and update the code to test getUserLeads
        //instance.getUserLeads(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putOww', function() {
      it('should call putOww successfully', function(done) {
        //uncomment below and update the code to test putOww
        //instance.putOww(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putUser', function() {
      it('should call putUser successfully', function(done) {
        //uncomment below and update the code to test putUser
        //instance.putUser(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
