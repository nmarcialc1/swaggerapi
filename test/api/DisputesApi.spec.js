/**
 * DOVLY API
 * APIs used to integrate with Dovly
 *
 * OpenAPI spec version: 1.0
 * Contact: softwaredev@dovly.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.DovlyApi);
  }
}(this, function(expect, DovlyApi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new DovlyApi.DisputesApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('DisputesApi', function() {
    describe('deleteLetterCategories', function() {
      it('should call deleteLetterCategories successfully', function(done) {
        //uncomment below and update the code to test deleteLetterCategories
        //instance.deleteLetterCategories(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getAllDisputes', function() {
      it('should call getAllDisputes successfully', function(done) {
        //uncomment below and update the code to test getAllDisputes
        //instance.getAllDisputes(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getLetterCategories', function() {
      it('should call getLetterCategories successfully', function(done) {
        //uncomment below and update the code to test getLetterCategories
        //instance.getLetterCategories(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getReasons', function() {
      it('should call getReasons successfully', function(done) {
        //uncomment below and update the code to test getReasons
        //instance.getReasons(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('postLetterCategories', function() {
      it('should call postLetterCategories successfully', function(done) {
        //uncomment below and update the code to test postLetterCategories
        //instance.postLetterCategories(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putDispute', function() {
      it('should call putDispute successfully', function(done) {
        //uncomment below and update the code to test putDispute
        //instance.putDispute(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putLetterCategories', function() {
      it('should call putLetterCategories successfully', function(done) {
        //uncomment below and update the code to test putLetterCategories
        //instance.putLetterCategories(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('putSchedule', function() {
      it('should call putSchedule successfully', function(done) {
        //uncomment below and update the code to test putSchedule
        //instance.putSchedule(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
